const express = require('express');
const router = express.Router();
const courseControllers = require('../controllers/courseControllers')
const auth = require('../auth');

router.post('/', (req, res) => {
		const data ={
				course: req.body,
				isAdmin: auth.decode(req.headers.authorization).isAdmin

		}
		courseControllers.addCourse(data).then(result => res.send(result));


})


router.get('/all', (req,res) => {
	courseControllers.getAllCourses().then(result => res.send(result))
})


router.get('/', (req,res) => {
	courseControllers.getAllActive().then(result => res.send(result))
})



router.get("/:courseId", (req,res)=>{
	courseControllers.specificCourse(req.params)
	.then(result =>{
		res.send(result)
	})
})



router.put('/:courseId', auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		course:req.body,
		Id: req.params
	}

	courseControllers.updateCourse(data).then(result => res.send(result))

})


router.put('/:courseId', auth.verify, (req,res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin)	{
		courseControllers.updateCourse(req.params,req.body).then( result => res.send(result))
	} else{
		res.send(false)
	}

})



router.put('/:courseId/archive', auth.verify, (req,res) => {

const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin)	{
		courseControllers.archiveCourse(req.params).then( result => res.send(result))
	} else{
		res.send(false)
	}
})







module.exports = router;