const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');




router.post('/checkEmail', (req,res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result))
})



router.post('/register', (req,res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})


router.post('/login',(req,res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})



router.get('/details', auth.verify, (req,res) => {
	
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile({userId: userData.id}).then(result => res.send(result));
})



router.post('/enroll', (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	console.log(data)
	userController.enroll(data).then(result => res.send(result))
})





module.exports = router;