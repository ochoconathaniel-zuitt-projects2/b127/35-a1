const express = require('express');
const mongoose = require('mongoose');

const cors = require('cors');

const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');



const app = express();

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.mv91k.mongodb.net/Batch127_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})


mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use('/users', userRoutes);
app.use('/courses', courseRoutes);


app.listen(process.env.PORT || 4000, ()=> {
	console.log(`API is now online on port ${ process.env.PORT || 4000}`)
})
