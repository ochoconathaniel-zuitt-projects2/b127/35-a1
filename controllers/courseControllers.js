const Course = require('../models/Course');




module.exports.addCourse = (data) => {
	if(data.isAdmin){
		let newCourse = new Course ({
			name: data.course.name,
			description: data.course.description,
			price:data.course.price
		})

			return newCourse.save().then((course,error) =>{
					if(error){
						return false;
					}else{
						return true;
					}


			})
	}
	else {
		return false;
	}
}


module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}


module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}



//Retrieve SPECIFIC course
module.exports.specificCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}


module.exports.updateCourse = (reqParams,reqBody) =>
{
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdandUpdate(reqParams.courseId,reqBody.courseId, updatedCourse).then((course,error) => {
		if(error){
			return false;
		}
		else {
			return true;
		}
	})	
}




module.exports.archiveCourse = (reqParams) => {
	let archivedCourse = {
		isActive: false
	}
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}