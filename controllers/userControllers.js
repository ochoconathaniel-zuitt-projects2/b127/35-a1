const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const Course = require('../models/Course');


module.exports.checkEmailExists = (reqBody) => {
	return User.find({email:reqBody.email}).then(result => {
		if(result.length > 0){
			return true;

		}
		else{


			return false;


		}

	})
}

module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName:  reqBody.lastName,
		email: 	   reqBody.email,
		mobileNo:  reqBody.mobileNo,
		password:  bcrypt.hashSync(reqBody.password, 10)
		
	})

	return newUser.save().then((user, error)=> {
	
		if(error){
			console.log(error);
			return false;
		}else{
			return true;
		}
	})
}



module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){

				return { accessToken: auth.createAccessToken(result.toObject()) }

			} else{
				return false;
			}
		}



	})
}


module.exports.getProfile = (data) => {
	return User.findById(data.userId).then((result, error) => { 
		if(error) {
            console.log(error)
            return false;
        }

        return result;
	})


}




 module.exports.enroll = async (data) => {

 	let isUserUpdated = await User.findById(data.user).then(user => {
 		user.enrollments.push({ courseId : data.courseId})
 	})
return User.save().then((user,error) => {
	if(error){
		return false;
	} else {
		return true;
	}

})


let isCourseUpdated = await Course.findById(data.courseId).then(course => {
	course.enrollees.push({ userId: data.userId});

	return course.save().then((course,error) => {
		if (error){
			return false;

		} else {
			return true;
		}
	})
})



if(isUserUpdated && isCourseUpdated){
	return true;

} else {
	return false;
}



 }